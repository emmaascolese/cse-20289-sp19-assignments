#!/usr/bin/env python3
import os 
import sys
import re

def cut():
    result=[] 
    for y in open('/etc/passwd'):
        result.append(y.split(':')[4])
    for r in result:
        yield re.findall('[Uu]ser',r)
print(len([item for sublist in list(cut()) for item in sublist]))
