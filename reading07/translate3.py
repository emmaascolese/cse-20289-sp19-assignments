#!/usr/bin/env python3
import requests
import re

fil = requests.get('http://yld.me/raw/Hk1').text.splitlines() #list of strings split by lines 
lastnames = [line.split(',')[1] for line in fil] #list of only the last names 
bnames = [bname for bname in lastnames if bname.startswith('B')] #get only the last names that start with B 

for b in sorted(bnames): #iterate over the list to print the B last names individually 
    print(b)
