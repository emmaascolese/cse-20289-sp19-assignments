#!/usr/bin/env python3
import os
from collections import Counter 
result = os.popen('/bin/ls -l /etc').readlines()
second = sorted([seconds.split()[1] for seconds in result])
dic = {}
for line in second:
   dic[line] = second.count(line)

for key, count in dic.items():
   print('{:7} {}'.format(count, key)) 

