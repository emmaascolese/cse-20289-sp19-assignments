#!/bin/bash

q1_answer() {
    # TODO: List only the last five first names in sorted order
    curl -sLk https://yld.me/raw/Hk1  | sort | tail -5 | cut -d , -f 3

}

q2_answer() {
    # TODO: Count how many phone numbers end in an odd number
    curl -sLk https://yld.me/raw/Hk1 | cut -d , -f 4 | grep -c "[13579]$"

}

q3_answer() {
    # TODO: List the last names that contain at at least two of the same vowels
    curl -sLk https://yld.me/raw/Hk1 | cut -d , -f 2 | grep -E "([aeiou]).*\1{1}"

}

q4_answer() {
    # TODO: Count all the netids that are exactly 8 characters long
    curl -sLk https://yld.me/raw/Hk1 | cut -d , -f 1 | grep -Ec "[a-z]{8}"

}

q5_answer() {
    # TODO: List all the netids that contain all of the last name
    curl -sLk https://yld.me/raw/Hk1 | cut -d , -f 2 | tr A-Z a-z

}

q6_answer() {
    # TODO: List the first names of people who have both two consecutive
    # repeated letters in their netid and two consecutive repeated numbers in
    # their phone number
    curl -sLk https://yld.me/raw/Hk1 cut -d , -f 1,3,4| grep -E "([a-z])\1{1}"  | cut -d , -f 2

}
