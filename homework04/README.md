Homework 04
===========

Demographics
============

1. Overall, there have been increasingly more women in the CSE department since 2013. This doesn't suprise me as middle school/ high school aged girls are encouraged more and more to take up technology. Additionally, currently about a third of the CSE department is female, which is pretty good compared to some other universities. 
Ethnic diversity doesn't look too well spread out in any of the years looked at in demographics.py. Every year has been predominantly white, but so has Notre Dame's general enrollment. I think that around 60% white is lower than Notre Dame's general enrollment, so Computer Science is probably more ethnically diverse than other programs. Lacking ethnic diversity, in my opinion, is a Notre Dame issue, not a CSE issue. 

2. I think that the CSE department promotes a welcoming environment to its students. Instructors are careful not to generalize what types of people are CSE majors and many professors of mine regularly say "he or she" when speaking about hypothetical students, which I think is a great way to demonstrate delibrate inclusivity. I think that maybe the culture would improve if Notre Dame's general attitude towards diversity and gender norms changed, but I think the CSE department does a good job of being inclusive. 

Reddit.py
=========

1. Scripting in python is easier to interpret and write than scripting in shell because of its similarities to English.
Overall I think the two languages are very similar excpet shell is more of a list of commands that interacts heavily commands that can also be used on the command line whereas python comes with features like libraries.
We could have written this program in shell although it is easier to comprehend in python because of the easier syntax and relationship to English. 

2. Python is run through an interpreter so it never gets compiled whereas C++ gets compiled. Additionally, Python does not require explicitly defined types and C++ does. Both languages are high-level languages and are general purpose 
We could have done this in C++ but it would have been a lot more tedious because we'd have to deal with reading in the file with an istream and putting them into a data structure then going through the information to put it in a dictionary.
