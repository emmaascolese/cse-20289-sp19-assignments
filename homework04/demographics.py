#!/usr/bin/env python3

import collections
import os
import sys
import argparse
import requests

# Globals

URL     = 'https://yld.me/raw/MtP'
GENDERS = ('M', 'F')
ETHNICS = ('B', 'C', 'N', 'O', 'S', 'T', 'U')

# Functions

def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] URL

    -y  YEARS   Which years to display (default: all)
    -p          Display data as percentages.
    -G          Do not include gender information.
    -E          Do not include ethnic information.
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def load_demo_data(url=URL):
    #''' Load demographics from specified URL into dictionary '''
    r = requests.get(url)
    dic = collections.defaultdict(dict)
    r= r.text.splitlines()
    for line in r[1:]:
        years, gen, eth = line.split(",")
        dic[years][gen] = dic[years].get(gen,0)+1 
        dic[years][eth] = dic[years].get(eth,0)+1
    return dic

def dump_demo_data(data, years=None, percent=False, gender=True, ethnic=True):
    ''' Dump demographics data for the specified years and attributes '''
    if years == None:
        years = data.keys()
    dump_demo_years(years)
    dump_demo_separator(years, '=')
    if gender == True:
        dump_demo_gender(data, years, percent)
    if ethnic == True:
        dump_demo_ethnic(data, years, percent)

def dump_demo_separator(years, char='='):
    ''' Dump demographics separator '''
    print(char*8,end="")
    for year in years: 
        print(char*8,end="")
    print()

def dump_demo_years(years):
    ''' Dump demographics years information '''
    for year in years:
        print('\t{}'.format(year),end="")
    print()

def dump_demo_fields(data, years, fields, percent=False):
    ''' Dump demographics information (for particular fields) ''' 
    totals= collections.defaultdict(dict)
    if percent:
       for year in years:
           total=0
           for info  in fields:
               if info in data[year]:
                   total= total + data[year][info]
           totals[year]=total
       for info in fields: 
           print("{:>4}".format(info), end= '\t')
           for year in years:
               if info not in data[year]:
                   print("{:>4.1f}%".format(0),end="\t")
               else:
                   print("{:>4.1f}%".format(data[year][info]/totals[year]*100), end='\t')
           print()
    else:
       for info in fields:
           print("{:>4}".format(info),end='\t')
           for year in years:
               if info not in data[year]:
                   print("{:>4}".format("0"),end="\t")
               else:
                   print("{:>4}".format(data[year][info]),end="\t")
           print()

def dump_demo_gender(data, years, percent=False):
    ''' Dump demographics gender information '''
    dump_demo_fields(data, years, GENDERS, percent) 
    dump_demo_separator(years, '-')

def dump_demo_ethnic(data, years, percent=False):
    ''' Dump demographics ethnic information '''
    dump_demo_fields(data,years,ETHNICS, percent)
    dump_demo_separator(years, '-')

# Parse Command-line Options
args = sys.argv[1:]
percent = False
gender = True
ethnicity= True
years=None
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-y':
        years= sorted(args.pop(0).split(',')) 
    elif arg == '-p':
        percent=True
    elif arg == '-G':
        gender=False
    elif arg == '-E':
        ethnicity=False 
    elif arg == '-h':
        usage(0)
    else: 
        usage(1) 
if len(args) != 0:
    url=args.pop(0)
else:
    url=URL

data= load_demo_data(url)
if not years:
    years= data.keys()
# Main Execution
dump_demo_data(data, years, percent, gender , ethnicity)
