#!/usr/bin/env python3

import os
import sys
import pprint
import requests

# Globals
short    = False 
limit    = 10
titlelen = 60
orderby  = 'score'
URL      = None
ISGD_URL = 'http://is.gd/create.php'

# Functions

def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] URL_OR_SUBREDDIT

    -s          Shorten URLs using (default: False)
    -n LIMIT    Number of articles to display (default: 10)
    -o ORDERBY  Field to sort articles by (default: score)
    -t TITLELEN Truncate title to specified length (default: 60)
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def load_reddit_data(url=URL):
    ''' Load reddit data from specified URL into dictionary '''    
    headers  = {'user-agent': 'reddit-{}'.format(os.environ.get('USER', 'cse-20289-sp19'))} 
    response = requests.get(url, headers=headers)
    data = response.json()
    child = data['data']['children']
    return child 

def dump_reddit_data(data, limit=10, orderby='score', titlelen=60, shorten=False):
    ''' Dump reddit data based on specified attributes '''
    if orderby == 'score':
        rev = True
    else:
        rev = False
    data = sorted(data , key=lambda c: c['data'][orderby], reverse=rev)
    for index, child in enumerate(data[:limit],1):
        title = child['data']['title'][:titlelen]
        OB = child['data'][orderby]
        url = child['data']['url']
        if shorten:
           url= shorten_url(url)
        if index > 1: 
           print()
        print('{:4}.\t{} (Score: {})'.format(index,title,child['data']['score']))
        print('\t{}'.format(url))

def shorten_url(url):
    ''' Shorten URL using yld.me '''
    url = requests.get(ISGD_URL, params={'format': 'json', 'url': url})
    ur = url.json()
    ur = ur['shorturl']
    return  ur

# Parse Command-line Options

args = sys.argv[1:]
if len(args)== 0:
    usage(1)
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-s':
        short= True
    elif arg == '-n':
        limit= int(args.pop(0))
    elif arg == '-o':
        orderby = args.pop(0)
    elif arg =='-t':
        titlelen = int(args.pop(0)) 
    elif arg== '-h':
        usage(0)
    else:
        usage(1)

if len(args):
    item= args.pop(0).strip()
    if item.startswith('https'):
        URL =  item
    else: 
        URL = r'https://reddit.com/r/' + item + '/.json'

# Main Execution
data = load_reddit_data(URL)
dump_reddit_data(data, limit , orderby , titlelen , short)
