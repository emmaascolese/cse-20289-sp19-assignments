#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h> 
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>

//parse command line options 

int main( int argc, char *argv[]){
char * host; 
char * port; 
if ( argc < 3){ //not enough arguments 
	printf("Usage: ./ncat HOST PORT\n");
	return EXIT_FAILURE;
}
host= argv[1];
port= argv[2];

//create socket and connect to specified host and port 
struct addrinfo * results;
struct addrinfo hints= { .ai_family = AF_UNSPEC, .ai_socktype = SOCK_STREAM,};
int status;
if ((status= getaddrinfo(host, port, &hints, &results)) != 0){
	fprintf(stderr, "Could not look up %s:%s: %s\n", host, port, gai_strerror(status));
	return EXIT_FAILURE;
}

int client= -1;

if ((client = socket(results->ai_family, results->ai_socktype, results->ai_protocol)) < 0) {
	fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
}
if (connect(client, results->ai_addr, results->ai_addrlen) < 0) {
	close(client);
	client = -1;
}
freeaddrinfo(results);
if (client < 0) {
	fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
    	return EXIT_FAILURE;
}
else 
	printf("Connected to %s:%s\n", host, port);
FILE *client_file = fdopen(client, "w+");
if (!client_file) {
        fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
        close(client);
        return EXIT_FAILURE;
}
   char buffer[BUFSIZ];
   fgets(buffer, BUFSIZ, stdin);
   fputs(buffer, client_file);
  
//freeaddrinfo(results);
fclose(client_file);


return EXIT_SUCCESS;
}
