#!/bin/sh

# Functions

usage() {
    cat <<EOF
Usage: $(basename $0) [rotation]

This program will read from stdin and rotate the text by the specified
rotation.  If none is specified, then the default value is 13.
EOF
    exit $1
}

source_set() {
    echo $LOWERCASE$UPPERCASE
}

target_set() {
    echo $TARGETSET$TARGET2 
}

# Parse command-line options
#for arg in $@; do
#    if [ arg -eq [0-9]* ]; then 
#	ROTATE=arg		
#    fi
#done 

if [ $# -lt 1 ]; then 
	ROTATE=13 
else 
	ROTATE=$1
fi

if [ $ROTATE > 26 ]; then 
	ROTATE=$(($ROTATE%26))
fi 

if [ "$1" = "-h" ]; then
    usage 0
fi

LOWERCASE=abcdefghijklmnopqrstuvwxyz
UPPERCASE=ABCDEFGHIJKLMNOPQRSTUVWXYZ

HEAD=$(echo $LOWERCASE | cut -c 1-$ROTATE)
TAIL=$(echo $LOWERCASE | cut -c $((ROTATE+1))-)
TARGETSET=$TAIL$HEAD

TARGET2=$(echo $TARGETSET | tr [a-z] [A-Z])
# Filter pipeline

tr $(source_set) $(target_set)

