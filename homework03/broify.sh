#!/bin/sh

#Functions

usage() {
cat <<EOF
Usage: $(basename $0)

-d DELIM    Use this as the comment delimiter.
-W        Don't strip empty lines.
EOF
    exit 1
}

# Parse command-line options

DELIM="#"
WS="/^$/d"


while [ $# -gt 0 ]; do
    case $1 in
    "-d")
	DELIM=$2
	shift
        ;;
    "-W")
	WS=""
        ;;
    *)
	usage
        ;;
    esac
    shift
done

# Filter pipeline

sed -e "s|$DELIM.*$||" -e 's|\s*$||g' -e "$WS"
