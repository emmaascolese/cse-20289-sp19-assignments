#!/bin/sh

#Functions

usage() {
	cat <<EOF
Usage: $(basename $0)

  -c      CITY    Which city to search
  -s      STATE   Which state to search (Indiana)

If no CITY is specified, then all the zip codes for the STATE are displayed.
EOF
	exit 1
}

# Parse command-line options

URL=https://www.zipcodestogo.com/

STATE="Indiana"
CITY="" 
SLASH="/"
while [ $# -gt 0 ]; do
    case $1 in
    "-c")
	CITY=$2
	shift
    ;;
    "-s")
	STATE=$2
	STATE=$(echo $STATE | sed 's| |%20|' )
	shift
    ;;
    *) usage ;;
    esac
    shift
done

# Filter pipeline(s)

curl -sk $URL$STATE$SLASH | grep -E "$CITY/" | grep -Eo '[0-9]{5}/' | sed 's|/||' | sort | uniq
