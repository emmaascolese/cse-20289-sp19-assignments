Homework 03
===========

Activity 1: 

1. In order to get the command line line arguments I used a conditional to see if the number of command lines was less than one. In that case, the rotate variable was set to 13, and if that was false the input variable from $1 is read in and set to the rotate variable. Also, if the first command line is the -h flag, then usage is called. 

2. I constructed the source set by concatenated an uppercase variable with a lowercase variable. 

3. I constructed the target set using the rotate variable. The lowercase variable is piped into a cut funtion. For the first part of the alpabet, it's cut from 1 to the rotate variable. The second part is cut from rotate+1 to the end, so that the letter at the rotate variable isn't double counted. Then those two variables are concatenating with the second part of the alphabet first and the first part second. This creates the rotated version of the alphabet and thus the target set. 
That is translated to uppercase and concatenated lowercase first then uppercase.
4. To perform the encryption, the words to be encrypted are piped from the command line and the last line of the code translates from the source set to the target set.

Activity 2 

1. I parsed the command line with a while statement that shifts everytime it runs through. The while ends when the element of the command line that is currently on is 0. 

2. I removed comments with a sed command the searches for what the comment delimiter  variable is and replaces it with nothing. If they don't use the -d flag no case is entered and the delimiter is automatically set to "#". If they use the -d flag, whatever is directly after it will be the delimiter, and that will be searched for and replaced with nothing so effectively deleted. 

3. To remove empty lines I used sed to search for lines that start with nothing and end with nothing and then delete them. 

4. The command line options affect the main text processing by deciding the values of the variables that decide what the comment delimiter will be and if the bland lines will be deleted. If the -W flag is used the WS variable is set to nothing so the last sed did nothing. 

Activity 3

1. THe command line was parsed using a while statement that loops through while the number of command line arguments left to parse is greater than 0. After values for either of the variable are taken in theres a shift to the next command line argument and there's one at the end of the loop too. 

2. I extracted the zipcodes by grepping for digits 0 through 9 that are five characters with a slash at then end, which is the format of the zipcodes on the website. 

3. I filtered the state by adding the state variable to the end of the url. The city was filtered using a grep -E and the city variable with a slash at the end, because that's how it's presented on the website. When the state variable is read in any spaces are replaced with %20 because that's how spaces are represented in the url 
