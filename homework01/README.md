Homework 01
===========


Activity 1 
----------

|Command			  | Elapsed Time |
|---------------------------------|--------------|
|cp -r/usr/share/pixmaps images   |0.402 s 	 |
|mv images/pixmaps		  |0.006 s	 |
|mv pixmaps /tmp/eascoles-pixmaps |0.480 s	 |
|rm -r /tmp/eascoles-pixmaps/	  |0.003 s	 |

1)Renaming takes significatntly less time because renaming doesn't change the location of the file so the inode remains the same. Renaming only changes the pointer in the file. When a file is moved information has to relocate creating new Inodes, mv has to execute a copy and a remove.

2)Removing is faster than moving because removing data removes the inode. Moving deletes the inode and creates another one in the new location.


Activity 2
----------

1) bc < math.txt
2) bc < math.txt > results.txt
3) bc < math.txt 2> /dev/null > results.txt 
4) cat math.txt | bc
5) bx < math.txt 2> /dev/null |tee results.txt
6) Pipelining is more efficient because you minimize the number of processes needed to execute something because you only run each process in a pipeline one time, whereas in redirection it may have to run the process for every file.

Activity 3
----------

1) grep -o "/sbin/nologin" /etc/passwd | wc -l 
2) ps -A| grep "bash" | wc -l
3) du /etc 2> /dev/null | sort -nr |head
4) who | cut -d " " -f 1 | uniq | wc -l

Activity 4 
----------

1a) CNTRL C does not work, kill -9 PID  while the troll is running does not work
, CNTRL Z doesn't work 
1b) kill -9 PID when the program is not running kills the troll
2a) kill -9 $(ps x | grep /TROLL | cut -d " " -f 2)
2b) kill -9 PID, get the PID by typing ps x in the second terminal without the troll   
3)kill -10 PID gave me cool letters coming from the top of the screen. kill -12 PID gives Star Wars 
