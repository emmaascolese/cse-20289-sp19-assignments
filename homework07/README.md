# Homework 07

## Activity 1: Map Library

1. Briefly describe what memory you allocated in `entry_create` and what memory
   you deallocated in `entry_delete`. How did you handle the `recursive` flag?

In entry_create memory was allocated for an entry struct through malloc and for the key with strdup because the entry struct only allocated memory for a pointer to the key and that key could be on the stack so you want to allocate memory for it on the heap so it survives between function calls. In entry_delete, I only deallocated things that are stored on the heap, so the entry struct and and the key get deallocated. I handled the recursive call by deallocating all of the next pointers of the entry and looped until next was NULL.

2. Briefly describe what memory you allocated in `map_create` and what memory
   you deallocated in `map_delete`. How did you handle the internal entries?

In map create I allocated the map struct and the buckets and the stuff that's in the hashtable that is the entries by using entry create. All of those things were deallocated in map delete, the entries were deallocated by using entry delete. 

3. Briefly describe what memory you allocated, copied, and deallocated in
   `map_resize`. When is this function called and what exactly does it do to
   grow the map?

Memory is allocated for the buckets because we're making a different array of buckets so the map can be resized. The new buckets get copied into the map. The old buckets need to get deallocated. Resize is called in insert if the load factor is over what it's supposed to be and it'll double the capacity so that there are double the amount of buckets made. 


4. Briefly describe how your `map_insert` function works and analyze its **time
   complexity** and **space complexity** in terms of **Big-O notation** (both
   average and worst case).

Map insert finds the bucket that the entry would be in, if that entry already exists it gets updated, otherwise it creates a new entry at the end of the linked list. It increments the size if a new entry is added. Finding the bucket is O(1) for time complexity because a hashtable is a lookup table and the bucket can be directly referenced, for both average case and worst case. Space complexity is not great because low load factors lead to lots of empty space in the hashtable.  Looking for the key in the linked list should be pretty good time complexity because of the load factor, so it's about constant time as the load factor is smaller. However, worst case time complexity is O(n), if the capactiy was one. In regards of space complexity, searching for the key is O(1) because nothing is getting changed, just looked for. But when you add something it's O(n) always because the entries are all the same size for space complexity.


5. Briefly describe how your `map_search` function works and analyze its **time
   complexity** and **space complexity** in terms of **Big-O notation** (both
   average and worst case).

Looking for the key in the correct bucket is what mapsearch does. It has the time and space complexity of looking for the key stated above. 

6. Briefly describe how your `map_remove` function works and analyze its **time
   complexity** and **space complexity** in terms of Big-O notation (both
   average and worst case).

Map remove gets rid of the entry with a specified key by setting the pointer of the entry before it to next pointer of the entry then calling entry delete on that entry. Time complexity average O(1) and worst is O(n) because you have to look for it to delete it. Space complexity is constant for both, because in remove more memory is never used than was being used before. 
   
## Activity 2: Freq Utility

1. Based on your experiments, what is the overall trend in terms of **time**
   and **space** as the number of items increases for the different **load
   factors**?

    Are the results surprising to you? Explain why or why not.

Time is constant for the different numbers, which is predictable because we predicted time complexity would be constant. Space complexity was largely constant because we predicted it to be mostly constant. Resize is the only function call that changes time and space complexity because all of the buckets have to be copied and replace the old buckets, since the load factor is increased the chances we'll resize decrease. 

    
2. There is no such thing as a **perfect data structure**. Compare this **hash
   table** to the **AVL tree** discussed in your data structures class. What
   are the advantages and disadvantages of both? If you had to use one as the
   underlying data structure for a **map**, which one would you choose and why?

Hash Table advantages: search is always constant time if resizing is utilized
Hash Table disadvantages: takes up a lot of space because you have to store the pointers 

AVL Tree advantages: less memory used because you only havve to store the values themselves 
AVL Tree disadvantages: lookup time is O(logn)

It depends on what advantages I needed to optimize when deciding which data structure I would use. Because this assignment has a lot of searching and inserting, a hashmap is better. 

    **Note**: If you are not currently in the data structures class, you can
    simply compare your **hash table** to any other **binary search tree** you
    are familiar with such as a **red-black tree**.
