/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	    map_create(size_t capacity, double load_factor) {
    Map * m= calloc(1,sizeof(Map));
    if ( m != NULL){
    if( capacity == 0)
        m->capacity = DEFAULT_CAPACITY;
    else
        m->capacity = capacity;
    if ( load_factor <= 0)
        m->load_factor = DEFAULT_LOAD_FACTOR;
    else 
        m->load_factor = load_factor;
    m->size=0;
    m->buckets = calloc(m->capacity,sizeof(Entry));
    }
    return m;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	    map_delete(Map *m) {
    for(int i=0; i < m->capacity ; i++){
        entry_delete(m->buckets[i].next , 1); 
    }
    free(m->buckets);
    free(m);
    return m;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void        map_insert(Map *m, const char *key, const Value value, Type type) {
    double ld = m->size / (double)m->capacity; 
    if ( m->load_factor < ld )
        map_resize(m, 2*(m->capacity));

    uint64_t k = fnv_hash(key, strlen(key)) % m->capacity; 
    Entry * e = m->buckets + k;
    while( e->next ){
        e = e->next;
        if ( strcmp(e->key, key) == 0 ){ //if the key is already in the bucket, update
            entry_update( e, value, type);
            return; 
        }
    }
        //broke out of the while, so we're at the end of the list, insert the entry there
        e->next = entry_create(key, value, NULL , type);
        m->size = m->size + 1;
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *     map_search(Map *m, const char *key) {
    uint64_t k = fnv_hash(key, strlen(key)) % m->capacity;
    Entry * e = m->buckets+k;
    while  (e->next){
        e = e->next;
        if (strcmp(e->key, key) == 0){
            return e ;
        }
    }
    
    return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool        map_remove(Map *m, const char *key) {
    uint64_t k= fnv_hash(key, strlen(key)) % m->capacity;
    Entry * prev = m->buckets+k;
    for (Entry * curr = m->buckets[k].next; curr ; curr = curr->next){
        if (strcmp(curr->key, key) == 0){
            prev->next = curr->next;
            curr = entry_delete(curr, 0);
            m->size = m->size - 1;
            return true;
        }
        prev=curr;
    } 
return false;
}

/**asfa 
 * Format all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void	    map_format(Map *m, FILE *stream, const Format mode) {
    for (size_t i=0; (i < m->capacity) ; i++){
        for( Entry * e = m->buckets[i].next ; e ; e = e->next)
            entry_format( e, stream, mode);
    }
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void        map_resize(Map *m, size_t new_capacity) {
    Entry * buckets = calloc(new_capacity, sizeof(Entry)); 
    Entry * oldb = m->buckets;
    for (size_t i=0; i < m->capacity ; i++){
        Entry * prev= NULL; 
        for (Entry * e = m->buckets[i].next ; e ; e = prev){
            prev = e->next; 
            int newb = fnv_hash(e->key, strlen(e->key)) % new_capacity; 
            e->next = buckets[newb].next; 
            buckets[newb].next= e;
        }
    }
    m->buckets = buckets;
    free(oldb);
    m->capacity = new_capacity;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
