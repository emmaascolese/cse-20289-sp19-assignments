/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, Format mode) {
    /* TODO: Use map library to compute frequencies of data from stream */
    char buf[BUFSIZ]; 
    Map * m = map_create((double)0, load_factor);
    while ( fscanf(stream, "%s", buf) != EOF ){
        Entry *  e = map_search(m, buf); 
        if (e)
            entry_update(e, (Value)(e->value.number+1), NUMBER); 
        else   map_insert(m, buf, (Value)1L, NUMBER); 
    }
    map_format(m, stdout, mode);
    map_delete(m); 
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* TODO: Parse command line arguments */
    double load = DEFAULT_LOAD_FACTOR;
    Format mode = VALUE_KEY;
    int argind = 1; 
    char *word; 
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];
        switch(arg[1]){ 
            case 'h':
                usage(0);
                break;
            case 'f':
                word = argv[argind]++;
                if ( !strcmp(word,"KEY")) mode = KEY; 
                else if (strcmp(word,"VALUE") == 0) mode = VALUE;
                else if ( !strcmp(word,"KEY_VALUE")) mode = KEY_VALUE;
                else if ( !strcmp(word,"VALUE_KEY")) mode = VALUE_KEY;
                else usage(1);
                break;
            case 'l':
                load = strtod(argv[argind++],NULL);
                break;
            default: 
                usage(1);
                break;
        }
    }

    /* TODO: Compute frequencies of data from stdin */
    freq_stream(stdin, load, mode);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
