# Homework 09

1. As discussed in class, working with **system calls** can be tricky because
   unlike most normal functions, they can fail.  For each of the following
   scenarios involving `doit`, identify which **system calls** would fail (if
   any) and described you handled that situation:

    - No arguments passed to `moveit`.
	It would cause save files, the system call written by me to fail because I pass argv[1], which wouldn't exist, so a segfault would occur, so if they pass no arguments then a usage message appears and the program exits with and error code 

    - `$EDITOR` environmental variable is not set.
	getenv will fail. If getenv fails, then I set the editor to vim 

    - User has run out of processes.
	fork would fail if the number of processes is at its max, because another process cannot be created. This was accounted for by printing an error message and causing the editfiles function to return false, which in main causes the program to exit to exit with an exit failure. 

    - `$EDITOR` program is not found.
	exelcp will fail because the environmental variable that was assignmed is a program that can not be run, so when attempting to change the identity of the child to run with the non-existant editor, there will be an error and edit files will return false, so then the program will end with an exit failure in main

    - Temporary file is deleted before moving files.
	move files will fail when you try to open the path that doesn't exist and an error message will print, movefiles will return false, and the program will exit with an exit failure in main, and if that is the case, the path is unlinked, freed and the program ends with a failure 

    - Destination path is not writable.
	if the destination path is not writable, rename will fail, and move files will print an error and return false, close the file that was being read from, and return false to main which will cause the program to exit with an exit failure  

2. As described in the project write-up, the **parent** is doing most of the
   work in `timeit` since it `forks`, `times`, and `waits` for the **child**
   (and possibly kills it), while the child simply calls `exec`.  To distribute
   the work more evenly, **Logan** proposes the following change:

    > Have the **child** set an `alarm` that goes off after the specified
    > `timeout`.  In the signal handler for the `alarm`, simply call `exit`, to
    > terminate the **child**.  This way, the **parent** just needs to `wait`
    > and doesn't need to perform a `kill` (since the **child** will terminate
    > itself after the `timeout`).

    Is this a good idea?  Explain why or why not.

	This isn't a good idea, because when an exec() is called a new address space is created, and that address space doesn't have information about the alarm so the alarm would never notify the parent of when it's done
