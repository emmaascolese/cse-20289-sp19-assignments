/* moveit.c: Interactive Move Command */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

/* Macros */

#define	streq(a, b) (strcmp(a, b) == 0)
#define strchomp(s) (s)[strlen(s) - 1] = 0

/* Functions */

/**
 * Display usage message and exit.
 * @param   progname    Program name.
 * @param   status      Exit status.
 */
void	usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s files...\n", progname);
    exit(status);
}

/**
 * Save list of file paths to temporary file.
 * @param   files       Array of path strings.
 * @param   n           Number of path strings.
 * @return  Newly allocated path to temporary file.
 */
char *	save_files(char **files, size_t n) {
    /* Create temporary file */
    char temp[] = "temp-XXXXXX";
    int fd; 
    if( (fd = mkstemp(temp)) == -1){
        fprintf(stderr, "Error on mkestemp: %s\n", strerror(errno));
        _exit(EXIT_FAILURE); 
    } 
    
    FILE *f; 
    f = fdopen(fd, "w"); //want to open it to write to 
    if (!f){
        fprintf(stderr, "Error on open: %s\n", strerror(errno));
        fclose(f);
        _exit(EXIT_FAILURE); 
    }
    /* Write list of file paths to temporary file */
    for(size_t i = 0; i<n; i++) {
       fprintf(f, "%s\n", files[i]);
    }
    fclose(f); 
    return strdup(temp);
}

/**
 * Run $EDITOR on specified path.
 * @param   path        Path to file to edit.
 * @return  Whether or not the $EDITOR process terminated successfully.
 */
bool	edit_files(const char *path) {
    /* Determine editor from environment */
    char * editor= getenv("EDITOR");
    /* Fork child process and execute editor on specified path;
     * Parent waits for child and checks exit status of child process */
    if(!editor)
        editor="vim"; 
    int status = 1;
    pid_t pid = fork();
    //char name[BUFSIZ];
    //snprintf(name, BUFSIZ, "%s %s", editor, path);
    if (pid < 0){
        fprintf(stderr, "Error on fork: %s", strerror(errno));
        return false;
    }
    else if (pid == 0){
        if(execlp(editor, editor, path,(char*) NULL)<0){
            fprintf(stderr, "Error on execlp: %s", strerror(errno));
            return false;
        }

    }
    else {
        while((pid=wait(&status))<0);
    }    
    return status;
}

/**
 * Rename files as specified in contents of path.
 * @param   files       Array of old path names.
 * @param   n           Number of old path names.
 * @param   path        Path to file with new names.
 * @return  Whether or not all rename operations were successful.
 */
bool	move_files(char **files, size_t n, const char *path) {
    /* Open specified path and read new name of files */
    FILE *tfs= fopen(path, "r");
    if ( tfs == NULL) {
        fprintf(stderr, "Error on fopen: %s", strerror(errno));
        return false;
    } 
    /* For each file argument and new file name, attempt to rename if the names
     * don't match */
    char newp[BUFSIZ];
    size_t i = 0; 
    while(fgets(newp, BUFSIZ, tfs) && i < n){ //with fgets you always get the new line
        strchomp(newp);
        if(!streq(files[i], newp)){ 
            if(rename(files[i], newp)<0){
                fprintf(stderr, "Error on rename: %s\n", strerror(errno));
                fclose(tfs);
                return false;
            } 
        }
        i++;
    }
    fclose(tfs); 
    return true;
}

/* Main Execution */

int	main(int argc, char *argv[]) {
    /* Parse command line options */
    if (argc == 1)
        usage(argv[0], 1);
    if (streq(argv[1], "-h"))
        usage(argv[0], 0);
    /* Save files */
    char * tpath = save_files(&argv[1], argc-1);
    /* Edit files */
    if (edit_files(tpath)){
        unlink(tpath);
        free(tpath);
        return EXIT_FAILURE;
    }
    /* Move files */
    if(!move_files(&argv[1], argc-1, tpath)){ 
        unlink(tpath);
        free(tpath);
        return EXIT_FAILURE;
    }
    if(unlink(tpath)<0)
        return EXIT_FAILURE;
    free(tpath);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
