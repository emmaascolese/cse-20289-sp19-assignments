/* moveit.c: Interactive Move Command */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>

/* Macros */

#define	streq(a, b) (strcmp(a, b) == 0)
#define strchomp(s) (s)[strlen(s) - 1] = 0
#define debug(M, ...) \
    if (Verbose) { \
        fprintf(stderr, "%s:%d:%s: " M, __FILE__, __LINE__, __func__, ##__VA_ARGS__); \
    }

/* Globals */

int  Timeout = 10;
bool Verbose = false;

/* Functions */

/**
 * Display usage message and exit.
 * @param   progname    Program name.
 * @param   status      Exit status.
 */
void	usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [options] command...\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -t SECONDS  Timeout duration before killing command (default is %d)\n", Timeout);
    fprintf(stderr, "    -v          Display verbose debugging output\n");
    exit(status);
}

/**
 * Parse command line options.
 * @param   argc        Number of command line arguments.
 * @param   argv        Array of command line argument strings.
 * @return  Array of strings representing command to execute.
 */
char ** parse_options(int argc, char **argv) {
    int argind=1; 
    char * PROG_NAME = argv[0];
    if (argc == argind)
        usage(PROG_NAME, 1);
    while ( argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char * arg = argv[argind++]; 
        if (streq(arg, "-h"))
            usage(PROG_NAME, 0);
        else if(streq(arg, "-t")){
            Timeout = atoi(argv[argind++]); 
        }
        else if (streq(arg, "-v"))
            Verbose = true;
        else
            usage(PROG_NAME, 1);
    }
    debug("Timeout = %d\n", Timeout);
    debug("Verbose = %d\n", Verbose);

    if( argc == argind)
        usage(PROG_NAME, 1); 

    int n = argc - argind + 1; //the plus one is for the null  
    char **command = calloc(n , sizeof(char *));

    for (int i = 0; i< (n-1); i++) 
        command[i] = argv[argind++];

    if(command[0]) debug("Command = %s", command[0]);
    for ( int i = 1; i<(n-1); i++)
        if (Verbose) fprintf(stderr, " %s", command[i]);
    if(command[0] && command[1] && Verbose) fprintf(stderr, "\n");
    return command;
}

/**
 * Handle signal.
 * @param   signum      Signal number.
 */
void    handle_signal(int signum) {
    debug("Received interrupt: %d\n", signum);
}

/* Main Execution */

int	main(int argc, char *argv[]) {
    
    struct timespec start, end; 
    /* Parse command line options */
    char ** commands = parse_options(argc, argv);

    /* Register handlers */
    signal(SIGCHLD, handle_signal);
    debug("Registering handlers...\n");

    /* Fork child process... parent can either sleep and kill or set alarm */
    int status;
    clock_gettime(CLOCK_MONOTONIC, &start); 
    debug("Grabbing start time...\n");
    pid_t pid = fork();
    if (pid < 0){
        fprintf(stderr, "Error on pid: %s\n", strerror(errno));
        free(commands);
        return EXIT_FAILURE;
    }
    else if (pid == 0){
        debug("Executing child...\n");
        int check = execvp(*commands, commands);
        if(check<0){
            fprintf(stderr, "Error on exec: %s\n", strerror(errno));
            free(commands);
            return EXIT_FAILURE;
        }
    }
    else { //parents

        debug("Sleeping for %d seconds...\n", Timeout);
        if(nanosleep((const struct timespec[]){{Timeout,0}},NULL)==0){
            debug("Killing child %d...\n", pid);
            kill(pid, SIGKILL);
        }
        debug("Waiting for child %d...\n", pid);
        while ((pid = wait(&status))<0);
        status = WIFSIGNALED(status) ? WTERMSIG(status) : WEXITSTATUS(status);
        debug("Child exit status: %d\n", status);
    }
    
    clock_gettime(CLOCK_MONOTONIC, &end); 
    debug("Grabbing end time...\n");
         
    float total = end.tv_sec - start.tv_sec; 

    printf("Time Elapsed: %.1f\n\n", total);
    free(commands);    
    WIFEXITED(status);
    return status;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */


