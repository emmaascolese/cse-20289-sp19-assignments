Homework 05
===========

I generated all the candidate password combinations by using the permuatations function and combining that with the prefix.

I filtered the candidates to only include valid passwords by taking the sha1sum of the candidates and checking if they were in hashes. If they were then they are valid passwords. 

I handled processing on multiple cores by using parallel computing. The way I did this was by utilizing multiprocessing and the pool function to gain access to multiple cores. After gaining access to the multiple cores, I created a subsmash variable that separated the data into different groups so they could be processsed on various cores. Then they results from those are put back together into the pas variable

I verified that my code workds properly by running all the doctests and then the makefile. At certain points in my coding process I also printed out different variables to make sure there was information in my HASHES and my passwords. 

Processes                   Time
===================================
1		   2 minutes 19.478s

2		   1 minute 12.690s 

4		   0 minutes 35.232s

8		   0 minutes 20.540s 

The more processes utilized the less time required to crack passwords

3. A longer password password will increase the amount of time compared to increasing the alphabet because adding another spot in a permutation for this example would increase the number of permutations by a factor of 36 ( the number of characters in our alphabet) whereas adding one letter to the alphabet does not increase the number of passwords to crack by nearly as much. 
