/* library.c: string utilities library */

#include "str.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	str_lower(char *s) {
    for (char *c=s; *c; c++)
        *c=tolower(*c);
    return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	str_upper(char *s) {
    for (char *c=s; *c; c++)
        *c=toupper(*c);
    return s;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	str_chomp(char *s) {
    if (s[strlen(s)-1]=='\n')
        s[strlen(s)-1]=0;
    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	str_strip(char *s) {
    char *head= s;
    char *write=s;
    while(isspace(*head))
        head++;
    while(*head){ 
        *write=*head; 
        write++, head++;
    }
    *write='\0';
    size_t sz=strlen(s);
    char *tail= s + sz - 1; 
    while(isspace(*tail))
        tail--;
    *(tail+1)='\0';
    return s;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	str_reverse(char *s) {
    char *head= s;
    size_t sz= strlen(s);
    char *tail= s + sz -1; 
    while(head < tail){
        char temp= *head;
        *head=*tail;
        *tail= temp; 
        head++, tail--;
    }
    return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	str_translate(char *s, char *from, char *to) {
    //from are the keys 
    int table[256]={0};
    char *temp= to; 
    size_t szf = strlen(from); 
    size_t szt = strlen(to);

    if ( szf != szt )
        return s;

    for(char * c=from; *c; c++){ 
        table[(size_t)*c] = *temp; //set the ASCII value for the letter in from to the corresponding letter in to
        temp++;
    }
    

    for(char *let=s; *let; let++){
        if(table[(size_t)*let] != 0) //check if the value is in from
            *let= table[(size_t)*let]; //set the letter in the string to the to value 
    }

    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	str_to_int(const char *s, int base) {
    int b=1, i=0;
    for(const char *c=(s+strlen(s)-1); c>=s; c--){    
        if((*c)>64 && (*c)<71)
            i= i+(*c-55)*(b);
        else if ((*c)>96 && (*c)<103)
            i=i+(*c-87)*(b);
        else
            i = i+(*c-48)*(b);
        b=b*base; 
    }
    return i;
}


/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
