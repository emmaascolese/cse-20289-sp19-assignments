/* main.c: string library utility */

#include "str.h"
#include "library.c"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Modes */

enum {
    /* TODO: Enumerate Modes */
    STRIP = 1<<1,
    REV= 1<<2,
    LOW= 1<<3,
    UP= 1<<4
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s      Strip whitespace\n");
    fprintf(stderr, "   -r      Reverse line\n");
    fprintf(stderr, "   -l      Convert to lowercase\n");
    fprintf(stderr, "   -u      Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    /* TODO: Process each line in stream by performing transformations */
    char buffer[BUFSIZ];

    while(fgets(buffer, BUFSIZ, stream)){
        str_chomp(buffer);
        if( *source && *target) str_translate(buffer, source, target);
        if (mode & STRIP) str_strip(buffer);
        if (mode & REV) str_reverse(buffer);
        if (mode & LOW) str_lower(buffer);
        if (mode & UP) str_upper(buffer); 
        fputs(buffer, stdout);
        putc('\n',stdout);
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* TODO: Parse command line arguments */
   int argind=1, mode=0;
   while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 's':
                mode |= STRIP;
                break;
            case 'r':
                mode |= REV;
                break;
            case 'l':
                mode |= LOW;
                break;
            case 'u':
                mode |= UP;
                break;
            default:
                usage(1);
                break;
        }
    }
char *source="";
char *target=""; 
if( argc > argind){
    source= argv[argind];
    argind++;
    target= argv[argind];
} 
 /* TODO: Translate Stream */
translate_stream(stdin, source, target, mode);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
