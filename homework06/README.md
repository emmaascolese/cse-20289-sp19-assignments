# Homework 06

## Activity 1 - Questions

1. What is the difference between a shared library such as `libstr.so` and a
   static library such as `libstr.a`?
   
   Static libraries, after they're compiled, cannot be changed and are linked to the program. Every program that uses a static library must have a copy of the library when it compiles. Shared libraries are separate files outside of the executable and therefor only one copy of the library is needed. A dynamic library can be modified without having to recompile, whereas a static library has to be recompiled because the copy of it in all the files needs to be updated. Static libraries are safer because copies of the libraries are in the files and thus cannot be changed once compiled. 

2. Compare the sizes of `libstr.so` and `libstr.a`, which one is larger? Why?
The sizes are pretty similar, but libstr.so is bigger. This is because the shared library contains the information to make is sharable, wheras the static library doesn't need that information.  

## Activity 2 - Questions

1. What is the difference between a static executable such as `str-static`
   and a dynamic executable such as `str-dynamic`?
   A static executable has a copy of the libraries needed contained in the executable  made at compile time. Dynamic executables reference the libraries at run time, so the libraries needed for the executable aren't actually in the files. 

2. Compare the sizes of `str-static` and `str-dynamic`, which one is larger?
   Why?
str-static is so much bigger than str-dynamic because the executable has copies of the libraries needed in the files, whereas dynamic executables just reference the libraries instead of actually containing them. 

3. Login into a new shell and try to execute `str-dynamic`.  Why doesn't
   `str-dynamic` work?  Explain what you had to do in order for `str-dynamic`
   to actually run.
Since dynamic libraries need to reference libraries during run time you can't just run it on a new shell without exporting the libraries that need to be referenced to the new shell. This is because the computer searches for the dynamic library within $LD_LIBRARY_PATH but it can't find it in a new shell. So you have to export the environmental variable to allow the computer to find the library so it can reference it 

4. Login into a new shell and try to execute `str-static`.  Why does
   `str-static` work, but `str-dynamic` does not in a brand new shell session?
Str-static works because it has all the information it needs to run contained within itself, that being the libraries it needs as copies of those libraries. 
