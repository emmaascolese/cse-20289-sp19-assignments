/* search_main.c: Main Execution */

#include "search.h"

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse options */
    Options opt={0};
    opt.uid=-1;
    opt.gid=-1;
    char *root=".";
    options_parse( argc, argv, &root , &opt); 

    /* Check root */
    if (!filter(root, &opt))
        puts(root);    
    /* Walk root */
    walk(root, &opt);  
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
