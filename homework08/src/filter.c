/* search_filter.c: Filters */

#include "search.h"

#include <errno.h>
#include <string.h>

#include <fnmatch.h>
#include <unistd.h>

/* Internal Filter Functions */

bool        filter_access(const char *path, const struct stat *stat, const Options *options) {
    return options->access && (access(path, options->access) != 0);
}

bool        filter_type(const char *path, const struct stat *stat, const Options *options) {
    return options->type && ((stat->st_mode & S_IFMT) != options->type);
}

bool        filter_empty(const char *path, const struct stat *stat, const Options *options) {
    if (options->empty){
     switch (stat->st_mode & S_IFMT){
        case S_IFREG:
            if( stat->st_size > 0)
                return true;
            break;
        case S_IFDIR:
            return !(is_directory_empty(path));
            break;
        default:
            return true;
     }
    }
    return false;
}

bool        filter_name(const char *path, const struct stat *stat, const Options *options) {
    char *name = strrchr(path, '/') + 1; //base name of the function
    return options->name && fnmatch(options->name, name, 0) != 0;
}

bool        filter_path(const char *path, const struct stat *stat, const Options *options) { 
    return options->path && fnmatch(options->path, path, 0) != 0;
}

bool        filter_perm(const char *path, const struct stat *stat, const Options *options) {
    return options->perm && (((S_IRWXU | S_IRWXG | S_IRWXO) & stat->st_mode) != options->perm);
}

bool        filter_newer(const char *path, const struct stat *stat, const Options *options) {
    return options->newer && ( get_mtime(path) <= options->newer);
}

bool        filter_uid(const char *path, const struct stat *stat, const Options *options) {
    return options->uid >= 0 && stat->st_uid != options->uid; 
}

bool        filter_gid(const char *path, const struct stat *stat, const Options *options) {
    return options->gid >= 0 && stat->st_gid != options->gid;
}

FilterFunc FILTER_FUNCTIONS[] = {   /* Array of function pointers. */
    filter_access,
    filter_type,
    filter_empty,
    filter_name,
    filter_path,
    filter_perm,
    filter_newer,
    filter_uid,
    filter_gid,
    NULL,                           /* TODO: Add remaining functions to array */
};

/* External Filter Functions */

/**
 * Filter path based options.
 * @param   path        Path to file to filter.
 * @param   options     Pointer to Options structure.
 * @return  Whether or not the path should be filtered out (false means include
 * it in output, true means exclude it from output).
 **/
bool	    filter(const char *path, const Options *options) {
    struct stat s;
    lstat(path, &s); 
    for( size_t i = 0; FILTER_FUNCTIONS[i]; i++){
        if(FILTER_FUNCTIONS[i](path, &s, options)) //call the current function
            return true;
    }
    return false;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
