/* search_options.c: Options */

#include "search.h"
#include "string.h"
#include <unistd.h>

/* Options Functions */

/**
 * Display usage message and exit.
 * @param   progname        Name of program.
 * @param   status          Exit status.
 */
void        options_usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s PATH [OPTIONS]\n", progname);
    fprintf(stderr, "\nOptions:\n");
    fprintf(stderr, "    -help           Display this help message\n\n");
    fprintf(stderr, "    -executable     File is executable or directory is searchable to user\n");
    fprintf(stderr, "    -readable       File readable to user\n");
    fprintf(stderr, "    -writable       File is writable to user\n\n");
    fprintf(stderr, "    -type [f|d]     File is of type f for regular file or d for directory\n\n");
    fprintf(stderr, "    -empty          File or directory is empty\n\n");
    fprintf(stderr, "    -name  PATTERN  Base of file name matches shell PATTERN\n");
    fprintf(stderr, "    -path  PATTERN  Path of file matches shell PATTERN\n\n");
    fprintf(stderr, "    -perm  MODE     File's permission bits are exactly MODE (octal)\n");
    fprintf(stderr, "    -newer fILE     File was modified more recently than FILE\n\n");
    fprintf(stderr, "    -uid   N        File's numeric user ID is N\n");
    fprintf(stderr, "    -gid   N        File's numeric group ID is N\n");
    exit(status);
}

/**
 * Parse command-line options.
 * @param   argc            Number of command-line arguments.
 * @param   argv            Array of command-line arguments.
 * @param   root            Pointer to root string.
 * @param   options         Pointer to Options structure.
 * @return  Whether or not parsing the command-line options was successful.
 */
bool        options_parse(int argc, char **argv, char **root, Options *options) {
    int argind = 1;
    
    if(argc == argind){
        return true; 
    }
    if (argv[argind][0] != '-'){
        *root = argv [argind++]; 
    }
    char * PROG_NAME = argv[0];
    while ( argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        if (streq(arg, "-executable")) {
            options->access |= X_OK; //bitset
        }
        else if (streq(arg, "-readable")){
            options->access |= R_OK;
        }
        else if (streq(arg, "-writable")){
            options->access |= W_OK;
        }
        else if (streq(arg, "-type")){
            switch(argv[argind++][0]){
                case 'f': 
                    options->type = S_IFREG;
                    break;
                case 'd':
                    options->type = S_IFDIR; 
                    break;
            }
        }
        else if (streq(arg, "-empty")){
           options->empty = true;
        }  
        else if (streq(arg, "-name")){
            options->name = argv[argind++];
        }
        else if (streq(arg, "-path")){
            options->path = argv[argind++]; 
        }
        else if (streq(arg, "-perm")){
            options->perm = strtol(argv[argind++],NULL, 8);
        }
        else if (streq(arg, "-newer")){
            time_t time = get_mtime(argv[argind++]);
            options->newer = time;
        }
        else if (streq(arg, "-uid")){
            options->uid = strtol(argv[argind++], NULL, 10);
        }
        else if (streq(arg, "-gid")){
            options->gid = strtol(argv[argind++], NULL, 10);
        }
        else if (streq(arg, "-help")){
            options_usage(PROG_NAME, EXIT_SUCCESS);
        }
        else 
            options_usage(PROG_NAME, EXIT_FAILURE);
    }

    return true;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
