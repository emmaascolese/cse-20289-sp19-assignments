/* search_walk.c: Walk */

#include "search.h"

#include <errno.h>
#include <dirent.h>

/* Walk Functions */

/**
 * Recursively walk the root directory with specified options.
 * @param   root        Root directory path.
 * @param   options     User specified filter options.
 * @return  Whether or not walking this directory was successful.
 */
int         walk(const char *root, const Options *options) {
    DIR *d = opendir(root);
    if (!d){
        fprintf(stderr, "%s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    for (struct dirent *e = readdir(d); e; e = readdir(d)){
        char path[BUFSIZ];

        //skip current and parent
        if (streq(e->d_name, ".") || streq(e->d_name, "..")){
            continue; 
        }

        snprintf(path, BUFSIZ, "%s/%s", root, e->d_name); //form full path

        if(!filter(path, options)){
            puts(path);
        }

        //struct stat s;
        if(e->d_type == DT_DIR){
            walk(path, options); 
        }
    }
    closedir(d);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
