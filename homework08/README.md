# Homework 08

## Strace Output 

### `seach`
%time	seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
 60.24    0.015067           6      2613           lstat
 20.64    0.005161           7       746           getdents
  9.13    0.002283           6       394        21 openat
  7.73    0.001934           5       375           close
  1.00    0.000249           6        41           write
  0.64    0.000160           5        30           brk
  0.23    0.000057           7         8           mmap
  0.12    0.000029           7         4           mprotect
  0.07    0.000017           9         2           open
  0.06    0.000016           5         3           fstat
  0.06    0.000014          14         1           munmap
  0.04    0.000009           9         1         1 access
  0.02    0.000005           5         1           read
  0.02    0.000005           5         1         1 ioctl
  0.02    0.000004           4         1           arch_prctl
  0.00    0.000000           0         1           execve
------ ----------- ----------- --------- --------- ----------------
100.00    0.025010                  4222        23 total

## `find`

## Questions
%time	seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
 39.73    0.015012           5      3264           fcntl
 18.58    0.007021           5      1409           close
 14.15    0.005346           7       746           getdents
 11.41    0.004311           6       767           newfstatat
  6.35    0.002398           6       395        21 openat
  4.79    0.001810           5       385           fstat
  2.21    0.000837           7       115           write
  0.89    0.000338          14        24        13 open
  0.61    0.000231           9        25           mmap
  0.36    0.000136          10        14           mprotect
  0.25    0.000093          10         9           brk
  0.23    0.000088           9        10           read
  0.12    0.000046          12         4           munmap
  0.06    0.000022          11         2         2 statfs
  0.05    0.000020           7         3         2 ioctl
  0.05    0.000019          10         2         1 access
  0.03    0.000011           6         2           rt_sigaction
  0.02    0.000009           9         1           execve
  0.02    0.000009           9         1           fchdir
  0.02    0.000006           6         1           rt_sigprocmask
  0.01    0.000005           5         1           uname
  0.01    0.000005           5         1           getrlimit
  0.01    0.000005           5         1           arch_prctl
  0.01    0.000005           5         1           set_tid_address
  0.01    0.000005           5         1           set_robust_list
------ ----------- ----------- --------- --------- ----------------
100.00    0.037788                  7184        39 total

1. Describe the differences you see between the number and type of system calls
   used by your utility as compared to the standard Unix program.

	The search utility uses less system calls than find and has less unique calls. 

2. Did you notice anything surprising about the trace of your utility or the
   trace of the standard Unix program? Which implementations are faster or more
   efficient?  Explain.
	
	Find is slower than search, which is surprising because I would assume that the real system call would be better then the one I wrote. Find also has more errors, which surprised me because I thought they would be the same. Overall it's surprising because I thought they would run the same. 
