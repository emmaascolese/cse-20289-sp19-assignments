#!/bin/sh


if [ $# -ne 1 ] ; then 
  echo "Usage: $0 executable"
  exit 1
fi
 
FILE=$(readlink -f $1)

if [ ! -r $FILE ] ; then 
  echo $FILE " is not readable!"
  exit 1
fi

if [ ! -x $FILE ] ; then 
  echo $FILE " is not executable!"
  exit 1
fi 

STUFF=$(strings $FILE) 
for i in $STUFF; do
  RETURN=`$FILE $i`
  if [ $RETURN ]; then
	echo "Password is $i"
	echo "Token    is $RETURN"
	exit 0
  fi
done

echo "Unable to crack $FILE"
exit 1
