#!/bin/sh

# Check if there's no arguments
if [ $# -lt 1 ]; then 
	echo "Usage: $0"
	exit 1
fi

for arg in $@; do
  case $arg in 
	*.tgz | *.tar.gz)
	  if ! tar xzvf $arg ; then
	  	exit 1
	  fi
	  ;;
	*.tbz | *.tar.bz2)
	  if ! tar xvjf $arg ; then
		exit 1
	  fi
	  ;;
	*.txz | *.tar.xz)
	  if ! tar xfvJ $arg ; then
		exit 1
	  fi
	  ;;
	*.zip | *.jar)
	  if ! unzip -o $arg ; then 
		exit 1
	  fi
	  ;;
	*)
	  echo "Unknown archive format: " $1
	  exit 1
  esac
done
