Homework 02
===========

Activity 1
----------

1. I checked that there were no arguments using an if statement that started with $# and used the flag -eq to check if the number of inputs was equal to zero. If it was, the programs ends with an exit code of 1 and the message "too few arguments" is displayed. 

2. To handle multiple arguments I created a for loop that loops through all arguments inputted to check if they are in a readable format. 

3. I used tar for all the ones with tar in the name or the ones that were equivalent to one with tar in the name and I used unzip for the one's with unzip in the name. I found the rest of the commands including the flags using online resources.

4. I checked the result of each extraction with if statements that led to an error exit message if the extraction failed. 

Activity 2
-----------

1. I checked if the program was readable or executable by using the square brackets, which are the equivalent of the test command. I used the flag -r for readable and -x for executable and if the test returned false the program exited with an error code

2. I extracted all of the possible passwords using the strings command and the file from the command line assigned to a variable which then contained all the strings from the file 

3. I determined when the program was cracked when the command with the password and the file returned something. The "something" it returned was the token.

Activity 3
---------- 

1. My first step was to scan 'xavier.h4x0r.space for a HTTP port: 
	command:  nmap -Pn  xavier.h4x0r.space -p 9000-9999
	output: Starting Nmap 6.40 ( http://nmap.org ) at 2019-01-31 19:58 EST
Nmap scan report for xavier.h4x0r.space (129.74.160.130)
Host is up (0.00039s latency).
Not shown: 997 closed ports
PORT     STATE SERVICE
9206/tcp open  unknown
9322/tcp open  unknown
9803/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 0.07 seconds

2. I then tried to access the HTTP server 
	command: curl 129.74.160.130:9322
	output:  _________________________________________ 
/ Halt! Who goes there?                   \
|                                         |
| If you seek the ORACLE, you must come   |
| back and _REQUEST_ the DOORMAN at       |
| xavier.h4x0r.space:9322/NETID/PASSWORD! |
|                                         |
| To retrieve your PASSWORD you must      |
| first _FIND_ your LOCKBOX which is      |
| located somewhere in                    |
| ~pbui/pub/oracle/lockboxes.             |
|                                         |
| Once the LOCKBOX has been located, you  |
| must use your _CRACKING_ skills to      |
| _BRUTEFORCE_ the LOCKBOX program until  |
| it reveals both a PASSWORD and a TOKEN! |
|                                         |
\ Good luck!                              /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
3. Then I had to find my lockbox
	command: find ~pbui/pub/oracle/lockboxes| grep eascoles
	output: /escnfs/home/pbui/pub/oracle/lockboxes/fc698150/8ad23a1d/7d607dab/7a7fcc52/ecac9d82/eascoles.lockbox


4. Then I had to open up the lockbox to get the password, so I used the crack program I had already written 
	command: ./crack/sh /escnfs/home/pbui/pub/oracle/lockboxes/fc698150/8ad23a1d/7d607dab/7a7fcc52/ecac9d82/eascoles.lockbox
	output: Password is ZWFzY29sZXMK
		Token    is 3e98d5d8a63c7b46fd8fb8b04b8b318136865bc9


5. Put password into the url given 
	command:  xavier.h4x0r.space:9322/eascoles/ZWFzY29sZXMK
	output: / Ah yes, eascoles... I've been waiting   \
| for you.                                |
|                                         |
| The ORACLE looks forward to talking to  |
| you... but you must first authenticate  |
| yourself with our agent, BOBBIT, who    |
| will give you a MESSAGE for the ORACLE. |
|                                         |
| BOBBIT can be found hidden in plain     |
| sight on Slack. Simply send him a       |
| direct message in the form "!verify     |
| NETID TOKEN". Be sure to use your NETID |
| and the TOKEN you retrieved from the    |
| LOCKBOX.                                |
|                                         |
| Once you have the MESSAGE from BOBBIT,  |
| proceed to port 9803 and deliver the    |
| MESSAGE to the ORACLE.                  |
|                                         |
| Hurry! The ORACLE is wise, but she is   |
\ not patient!                            /
 -----------------------------------------

\                             .       .
 \                           / `.   .' "
  \                  .---.  <    > <    >  .---.
   \                 |    \  \ - ~ ~ - /  /    |
         _____          ..-~             ~-..-~
        |     |   \~~~\.'                    `./~~~/
       ---------   \__/                        \__/
      .'  O    \     /               /       \  "
     (_____,    `._.'               |         }  \/~~~/
      `----.          /       }     |        /    \__/
            `-.      |       /      |       /      `. ,~~|
                ~-.__|      /_ - ~ ^|      /- _      `..-'
                     |     /        |     /     ~-.     `-. _  _  _
                     |_____|        |_____|         ~ - . _ _ _ _ _>


6. Messaged the Bobbit and got the message from it

7. To give the oracle the message I did the following 
	command: nc  129.74.160.130 9803
	output: The oracle appeared and asked me for my name, which was my netid, my message from the bobbit and my reason 
