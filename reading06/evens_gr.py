#!/usr/bin/env python3

import sys

def evens(stream):
    #e = [(map(int, stream))]
    #yield e 

    for y in (filter(lambda x: x % 2 == 0, (map(int, stream)))):
        yield str(y)


print(' '.join(evens(sys.stdin)))
