#!/usr/bin/env python3 

import sys 

print (' '.join(
    str(y) for y in (filter(lambda x: x % 2 ==0, (map(int, sys.stdin))))
))
