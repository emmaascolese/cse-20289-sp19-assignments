#!/bin/bash

q1_answer() {
    # TODO: List only the names of the turtles in sorted order.
    curl -sLk https://yld.me/raw/lE8 | grep -Eo "^[A-Z]\w*:\b" | grep -Eo "^[A-Z]\w*[a-z]\b" | sort
 
}

q2_answer() {
    # TODO: List only the colors of the turtles in all capitals.
    curl -sLk https://yld.me/raw/lE8 | grep -Eo ":\w*:\b" | grep -Eo "[a-z]*[a-z]\b" | tr [a-z] [A-Z]

}

q3_answer() {
    # TODO: Replace all weapons with plowshares
    curl -sLk https://yld.me/raw/lE8 | sed 's/:\w*[a-z]$/:plowshare/g'

}

q4_answer() {
    # TODO: List only the turtles whose names end in lo.
    curl -sLk https://yld.me/raw/lE8 | grep -Eo "^[A-Z]\w*:\b" | grep -Eo "^[A-Z]\w*lo\b"

}

q5_answer() {
    # TODO: List only the turtles with names that have two consecutive vowels.
    curl -sLk https://yld.me/raw/lE8 | grep -Eo "^[A-Z]\w*:\b" | grep -Eo "^[A-Z]\w*[a-z]\b" | grep -E "[aeiou]{2}"

}

q6_answer() {
    # TODO: Count how many colors don't begin with a vowel
    curl -sLk https://yld.me/raw/lE8 | grep -Eo ":\w*:\b" | grep -Eo "[a-z]*[a-z]\b" | grep -c "^[^aeiou]"

}

q7_answer() {
    # TODO: List only the turtles names whose name ends with a vowel and whose weapon ends with a vowel.
    curl -sLk https://yld.me/raw/lE8 | grep -E ":\w.*[aeiou]$" | grep -Eo "^[A-Z]\w*[aeiou]\b"
}

q8_answer() {
    # TODO: List only the turtles names with two of the same consecutive letter (i.e. aa, bb, etc.)
    curl -sLk https://yld.me/raw/lE8 | grep -E "^[A-Z]\w*:\b" | grep -E "(.)\1{1}" | grep -Eo ":\w*:\b" |grep -Eo "[a-z]\w*[a-z]\b"

}
